import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  ActivityIndicator,
  TextInput,
  TouchableHighlight,
  Dimensions
} from 'react-native';
import { Rating } from 'react-native-elements';

export default class App extends React.Component {

  // Init
  constructor() {
    super()

    this.PAGE_SIZE = 10
    this.TOP_FREE_LINK = 'https://itunes.apple.com/hk/rss/topfreeapplications/limit=100/json'
    this.LOOKUP_PREFIX = 'https://itunes.apple.com/hk/lookup?id='
    this.TOP_RECOMMENDATION = 'https://itunes.apple.com/hk/rss/topgrossingapplications/limit=10/json'

    this.ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => (r1.id !== r2.id || r1.star !== r2.star || r1.star_rating_count !== r2.star_rating_count) })
    this.all_top_free = [] // orig top free (hidden)
    this.filtered_top_free = [] // filtered top free (hidden)
    this.display_top_free = []  // displayed top free (shown)
    this.recommendation = []

    this.state = {
      search_text: '',
      loaded_top_free: false,
      loaded_recommendation: false,
      error_top_free: false,
      error_top_recommendation: false,
      load_more: false,
      pending_fetch_star: 0,
      ds_recommendation: this.ds.cloneWithRows(this.recommendation),
      ds_display_top_free: this.ds.cloneWithRows(this.display_top_free),
    }

  }

  componentDidMount() {
    this.fetch_top_free()
    this.fetch_recommendation()
  }

  // Fetch
  fetch_top_free() {
    this.setState({
      loaded_top_free: false,
      error_top_free: false,
    })

    fetch(this.TOP_FREE_LINK)
      .then((response) => response.json())
      .then((responseJson) => {
        this.all_top_free = []

        for (var i = 0; i < responseJson.feed.entry.length; i++) {
          this.all_top_free.push(this.get_entry_detail(responseJson.feed.entry[i]))
        }

        this.filtered_top_free = this.all_top_free

        this.push_new_page()

        this.setState({
          loaded_top_free: true,
          error_top_free: false,
        })
      })
      .catch((error) => {
        this.setState({
          loaded_top_free: false,
          error_top_free: true,
        })

      })
  }

  fetch_recommendation() {
    this.setState({
      loaded_recommendation: false,
      error_recommendation: false,
    })

    fetch(this.TOP_RECOMMENDATION)
      .then((response) => response.json())
      .then((responseJson) => {

        this.recommendation = []

        for (var i = 0; i < responseJson.feed.entry.length; i++) {
          this.recommendation.push(this.get_entry_detail(responseJson.feed.entry[i]))
        }

        this.setState({
          ds_recommendation: this.ds.cloneWithRows(this.recommendation),
          loaded_recommendation: true,
          error_recommendation: false,
        })

      })
      .catch((error) => {
        this.setState({
          loaded_recommendation: false,
          error_recommendation: true,
        })
      })
  }

  // Utils
  find_display_index_from_id(id) {
    var result = -1
    for (var i = 0; i < this.display_top_free.length; i++) {
      if (this.display_top_free[i].id == id) {
        result = i
        break
      }
    }
    return result
  }

  get_entry_detail(entry) {

    var summary = entry.summary.label
    var summary_in_lower_case = summary.toLowerCase()

    return {
      id: entry.id.attributes['im:id'],
      name: entry['im:name'].label,
      image: entry['im:image'][2].label,
      category: entry.category.attributes.term,
      author: entry['im:artist'].label,
      summary: entry.summary.label.toLocaleLowerCase(), // toLocaleLowerCase = used to accelerate search & filter
      star: undefined,
      star_rating_count: undefined,
    }
  }

  // UI Utils
  fill_star_and_star_rating_count(input_id) {

    this.setState({
      pending_fetch_star: (this.state.pending_fetch_star + 1)
    });

    fetch(this.LOOKUP_PREFIX + input_id)
      .then((response) => {
        return JSON.parse(response._bodyText)
      })
      .then((responseJson) => {

        var id = responseJson.results[0].trackId
        var averageUserRating = responseJson.results[0].averageUserRating
        var userRatingCount = responseJson.results[0].userRatingCount

        var index = this.find_display_index_from_id(id)

        if (index != -1) {
          this.display_top_free[index].star = averageUserRating
          this.display_top_free[index].star_rating_count = userRatingCount

          this.setState({
            pending_fetch_star: (this.state.pending_fetch_star - 1)
          });

          if (this.state.pending_fetch_star == 0) {
            this.setState({
              ds_display_top_free: this.ds.cloneWithRows(this.display_top_free)
            })
          }


        }

      })
      .catch((error) => {
        this.setState({
          pending_fetch_star: (this.state.pending_fetch_star - 1)
        });
        if (this.state.pending_fetch_star == 0) {
          this.setState({
            ds_display_top_free: this.ds.cloneWithRows(this.display_top_free)
          })
        }
      })
  }

  push_new_page() {
    var start = this.display_top_free.length
    var end = start + this.PAGE_SIZE
    end = Math.min(end, this.filtered_top_free.length)

    this.display_top_free = this.display_top_free.concat(this.filtered_top_free.slice(start, end))

    for (var i = start; i < end; i++) {
      if (this.display_top_free[i].star === undefined || this.display_top_free[i].star_rating_count === undefined){
        this.fill_star_and_star_rating_count(this.display_top_free[i].id)
      }
      
    }

    this.setState({
      load_more: false,
      ds_display_top_free: this.ds.cloneWithRows(this.display_top_free)
    })
  }

  // Event
  onEndReached() {
    this.setState({
      load_more: (this.display_top_free.length < this.filtered_top_free.length),
    })

    if (this.state.load_more) {
      this.push_new_page()
    }
  }

  onChangeText(text) {
    this.setState({ search_text: text })

    text = text.trim().toLocaleLowerCase()

    var filtered_recommendation = null
    if (text.length == 0) {
      filtered_recommendation = this.recommendation
      this.filtered_top_free = this.all_top_free
    } else {
      {/* var filtered_recommendation = this.recommendation.filter(function (entry) {
                          return (entry.name.toLowerCase().indexOf(text.toLowerCase()) !== -1 ||
                            entry.author.toLowerCase().indexOf(text.toLowerCase()) !== -1 ||
                            entry.summary.toLowerCase().indexOf(text.toLowerCase()) !== -1
                          )
                          }); */}

      {/* this.filtered_top_free = this.all_top_free.filter(function (entry) {
                          return (entry.name.toLowerCase().indexOf(text.toLowerCase()) !== -1 ||
                            entry.author.toLowerCase().indexOf(text.toLowerCase()) !== -1 ||
                            entry.summary.toLowerCase().indexOf(text.toLowerCase()) !== -1
                          ) 
                          }); */}


      filtered_recommendation = this.recommendation.filter(function (entry) {
        return (entry.name.toLowerCase().includes(text) ||
          entry.author.toLowerCase().includes(text) ||
          entry.summary.includes(text)
        ) 
      }) 

      this.filtered_top_free = this.all_top_free.filter(function (entry) {
        return (entry.name.toLowerCase().includes(text) ||
          entry.author.toLowerCase().includes(text) ||
          entry.summary.includes(text)
        )

      })
    }

    this.setState({
      ds_recommendation: this.ds.cloneWithRows(filtered_recommendation)
    })

    this.display_top_free = []
    this.push_new_page()
  }

  // Render
  renderHeader() {

    return (<View>

      {this.state.ds_recommendation.getRowCount() != 0 &&

        <View
          style={styles.container_recommendation}>

          <Text
            style={styles.title_recommendation}
          >Recommendation</Text>

          <ListView
            style={styles.list_recommendation}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            enableEmptySections={true}
            dataSource={this.state.ds_recommendation}
            renderRow={(rowData, sectionID, rowID) => <View>

              <Image
                style={
                  (rowID == 0) ? styles.list_item_image_left_recommendation : ((rowID != (this.recommendation.length - 1)) ? styles.list_item_image_normal_recommendation : styles.list_item_image_right_recommendation)
                }
                source={{ uri: rowData.image }}
              />
              <Text
                style={
                  (rowID == 0) ? styles.list_item_name_left_recommendation : styles.list_item_name_normal_recommendation
                }
                numberOfLines={2}
              >{rowData.name}</Text>
              <Text style={
                (rowID == 0) ? styles.list_item_category_left_recommendation : styles.list_item_category_normal_recommendation
              }
                numberOfLines={2}
              >{rowData.category}</Text>
            </View>}
          />

        </View>
      }

    </View>)

  }

  renderFooter() {
    if (this.state.load_more) {
      return <View style={
        styles.footer
      }
      >
        <ActivityIndicator />
      </View>
    } else {
      return <View />
    }
  }

  render() {
    return (

      <View style={
        styles.the_most_top_container
      }>

        {!(this.state.loaded_top_free && this.state.loaded_recommendation) && // !(this.state.loaded_top_free && this.state.loaded_recommendation)
          <View>

            {(this.state.error_top_free || this.state.error_recommendation) && // (this.state.error_top_free || this.state.error_recommendation)
              <View style={
                styles.retry_container
              }>
                <TouchableHighlight
                  underlayColor='transparent'
                  onPress={() => {
                    if (this.state.error_top_free) {
                      this.fetch_top_free()
                    }

                    if (this.state.error_top_recommendation) {
                      this.fetch_recommendation()
                    }

                  }}
                >
                  <Text
                    style={
                      styles.retry_text
                    }>Retry</Text>
                </TouchableHighlight>

                <Text
                  style={
                    styles.network_error_text
                  }>Network error</Text>
              </View>

            }

            {!(this.state.error_top_free || this.state.error_recommendation) && // !(this.state.error_top_free || this.state.error_recommendation)
              <ActivityIndicator
              />
            }

          </View>
        }

        {(this.state.loaded_top_free && this.state.loaded_recommendation) && // (this.state.loaded_top_free && this.state.loaded_recommendation)
          <View style={styles.container}>

            <View
              style={styles.container_search_textinput}>
              <TextInput
                style={styles.search_textinput}
                value={this.state.search_text}
                clearButtonMode='while-editing'
                placeholder="Search"
                autoCapitalize='none'
                onChangeText={this.onChangeText.bind(this)}                
              />
            </View>

            <ListView
              dataSource={this.state.ds_display_top_free}
              enableEmptySections={true}
              renderHeader={this.renderHeader.bind(this)}
              renderFooter={this.renderFooter.bind(this)}
              renderSeparator={(sectionId, rowId) => <View key={rowId} style={
                styles.list_item_separator_top_free
              } />}
              onEndReached={this.onEndReached.bind(this)}
              renderRow={(rowData, sectionID, rowID) =>
                <View
                  style={styles.list_item_container_top_free}
                >
                  <Text
                    style={styles.list_item_index_top_free}>{parseInt(rowID) + 1}</Text>

                  <Image
                    style={
                      ((parseInt(rowID)+1) % 2 != 0) ? styles.list_item_image_odd_top_free : styles.list_item_image_even_top_free
                    } 
                    source={{ uri: rowData.image }}
                  />
                  <View
                    style={
                      styles.list_item_info_container_top_free
                    }
                  >
                    <Text
                      style={
                        styles.list_item_name_top_free
                      }
                      numberOfLines={1}
                    >{rowData.name}</Text>
                    <Text style={
                      styles.list_item_category_top_free
                    }>{rowData.category}</Text>
                    <View
                      style={
                        styles.list_item_star_container_top_free
                      }
                    >
                      {rowData.star !== undefined &&
                        <Rating
                          imageSize={12}
                          readonly
                          startingValue={rowData.star}
                          style={
                            styles.list_item_star_top_free
                          }
                        />
                      }
                      {rowData.star_rating_count !== undefined &&
                        <Text
                          style={
                            styles.list_item_star_rating_count_top_free
                          }>({rowData.star_rating_count})</Text>
                      }

                    </View>

                  </View>

                </View>
              }


            />
          </View>
        }

      </View>
    )
  }
}

export const colors = {
  color_primary: 'white',
  color_secondary: 'black',

  separator_color: '#E4E4E4',
  search_textinput_color: '#E4E4E4',

  retry_button_color: '#4D4D4D',
  index_color: '#888888',
  sub_title_color: '#797979',
  star_rating_count_color: '#666666',
}

const styles = StyleSheet.create({
  container_recommendation:
  {
    borderBottomColor: colors.separator_color,
    borderBottomWidth: 1,
  },

  title_recommendation:
  {
    marginLeft: 10,
    marginTop: 10,
    marginBottom: 10,
    fontWeight: 'bold',
  },

  list_recommendation:
  {
    paddingBottom: 16,
  },

  list_item_image_normal_recommendation:
  {
    width: 60,
    height: 60,
    borderRadius: 10,
    marginLeft: 5,
    marginRight: 5,
  },

  list_item_image_left_recommendation:
  {
    width: 60,
    height: 60,
    borderRadius: 10,
    marginLeft: 10,
    marginRight: 5,
  },

  list_item_image_right_recommendation:
  {
    width: 60,
    height: 60,
    borderRadius: 10,
    marginLeft: 5,
    marginRight: 10,
  },

  list_item_name_normal_recommendation:
  {
    width: 60,
    marginLeft: 5,
    fontSize: 12,
  },

  list_item_name_left_recommendation:
  {
    width: 60,
    marginLeft: 10,
    fontSize: 12,
  },

  list_item_category_normal_recommendation:
  {
    width: 60,
    marginLeft: 5,
    fontSize: 12,
    color: colors.sub_title_color,
  },

  list_item_category_left_recommendation:
  {
    width: 60,
    marginLeft: 10,
    fontSize: 12,
    color: colors.sub_title_color,
  },

  footer:
  {
    marginTop: 8,
    marginBottom: 8,
  },

  the_most_top_container:
  {
    backgroundColor: 'white',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  retry_container:
  {
    alignItems: 'center'
  },

  retry_text:
  {
    color: colors.retry_button_color,
    marginBottom: 10,
  },

  network_error_text:
  {
    color: 'red'

  },

  container:
  {
    marginTop: 20,
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height - 20,
  },

  container_search_textinput:
  {
    padding: 10,
    backgroundColor: 'transparent',
    borderBottomColor: colors.separator_color,
    borderBottomWidth: 1,
  },

  search_textinput:
  {
    height: 30,
    borderColor: colors.search_textinput_color,
    backgroundColor: colors.search_textinput_color,
    borderWidth: 1,
    borderRadius: 8,
    paddingLeft: 8,

  },

  list_item_container_top_free:
  {
    flexDirection: 'row',
    marginTop: 8,
    marginBottom: 8,

  },

  list_item_index_top_free:
  {
    width: 26,
    marginLeft: 16,
    color: colors.index_color,
    paddingLeft: 0,
    paddingTop: 18,
  },

  list_item_image_odd_top_free:
  {
    width: 50,
    height: 50,
    marginRight: 8,
    backgroundColor: 'white',
    borderRadius: 8,
  },

  list_item_image_even_top_free:
  {
    width: 50,
    height: 50,
    marginRight: 8,
    backgroundColor: 'white',
    borderRadius: 25,
  },

  list_item_info_container_top_free:
  {
    flexDirection: 'column',
    flex: 1,
  },

  list_item_name_top_free:
  {
    marginRight: 16,
    marginBottom: 2,
    color: 'black',

  },

  list_item_category_top_free:
  {
    color: colors.sub_title_color,
    fontSize: 12,
    marginBottom: 2,
  },

  list_item_star_container_top_free:
  {
    flexDirection: 'row',
  },

  list_item_star_top_free:
  {
    marginTop: 2,
    marginRight: 4,
  },

  list_item_star_rating_count_top_free:
  {
    color: colors.star_rating_count_color,
    fontSize: 12,
  },

  list_item_separator_top_free:
  {
    flex: 1,
    height: StyleSheet.hairlineWidth,
    backgroundColor: colors.separator_color,
    marginLeft: 16,
  },


})
